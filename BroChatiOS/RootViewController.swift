//
//  ViewController.swift
//  BroChatiOS
//
//  Created by Nicholas Galasso on 10/17/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import UIKit
import Socket_IO_Client_Swift
import CoreLocation

class RootViewController: UIViewController, CLLocationManagerDelegate {
    
    let chatTable = ChatTableView(frame: CGRectZero, style: .Plain)
    
    let label = UILabel()
    let socket = SocketIOClient(socketURL:"brochat.info:3000")
    let locMgr = CLLocationManager()
    var lastLoc:CLLocation? = CLLocation(latitude: 40.72, longitude: -73.95)
    var users:[User] = []
    var roomTitle:String?
    var messages:[Message] = []
    
    let userName = "Nick iOS"

    /*

    View Lifecycle
    
    */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locMgr.delegate = self
        
        self.requestLocationPermissionIfNeeded()
        
        self.setupSocket()
        
        self.label.text = "hello world"
        self.label.backgroundColor = UIColor.greenColor()
        self.label.textAlignment = .Center
        
//        self.view.addSubview(self.label)
        self.view.addSubview(self.chatTable)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.chatTable.frame = self.view.bounds
    }

    
    
    /*

    Location Handling
    
    */
    
    func requestLocationPermissionIfNeeded(){
        
        print(__FUNCTION__)
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            print("loc enabled")
            
            switch CLLocationManager.authorizationStatus() {
                
            case .Authorized:
                print("Authorized")
                self.locMgr.startUpdatingLocation()
                
            case .Denied:
                print("Denied")
                
            case .NotDetermined:
                print("Not Determined")
                self.locMgr.requestAlwaysAuthorization()    
                self.locMgr.startUpdatingLocation()
                
            case .Restricted:
                print("Restricted")
            
            default:
                print("unknown enum")
            }
            
        } else {
            print("loc disabled")
        }
        
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        print(__FUNCTION__)
        
        self.lastLoc = newLocation
        
        manager.stopUpdatingLocation()
        
        self.socket.connect()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(__FUNCTION__)
        print(error.localizedDescription)
    }

    
    /*

    Socket Handling
    
    */
    
    func setupSocket(){
        
        socket.on("connect") {data, ack in
            print("socket connected")
            print(data)
            
            if let currentLoc = self.lastLoc {
                
                let lat = "\(currentLoc.coordinate.latitude)"
                let lon = "\(currentLoc.coordinate.longitude)"
                
                let dict = [ "UserName" : self.userName,
                    "Lat" : lat,
                    "Lon" : lon]
                
                self.socket.emit("initialize", dict)
                
            } else {
                
                print("no location available, did not connect")
            }
        }
        
        socket.on("title") {data, ack in

            if let roomName = data.first as? String {
                print("got room title: " + roomName)
                self.roomTitle = roomName
            }
        }
        
        socket.on("usersInRoomUpdate") { data, ack in

            if let users = data.first as? [Dictionary<String, String>]{

                self.users.removeAll(keepCapacity: true)

                for dict in users {
                    if let username = dict["Name"] {
                        print(username)
                        self.users.append(User(username: username))
                    }
                }
            }
        }
        
        socket.on("chatLoaded") { data, ack in
            
            print("chat loaded")
            print(data)
        }
        
        socket.on("message") { data, ack in

            if let msgStr = data.first as? String {
                
                self.chatTable.addMessage(Message(str:msgStr))
            }
        }
        
//        
//        socket.on("messageHistory"){ data in
//            
//            if let msgArray = data as? [AnyObject]{
//                
//                for dict in msgArray{
//                    if let content:String = dict["Content"] as? String {
//                        self.addMessage(content)
//                    }
//                }
//            }
//        }
//        
//        //
//        socket.on("selfMessage") { data in
//            
//            if let msg = data as? String {
//                
//                print(msg)
//                self.addMessage(msg)
//            }
//        }
//        
//        socket.on("weather") { data in
//            
//            if let weatherDict = data as? Dictionary<String, String> {
//                
//                var mut = ""
//                
//                if let tempStr = weatherDict["Temp"] {
//                    mut = mut + tempStr
//                }
//                
//                if let weatherStr = weatherDict["Weather"] {
//                    mut = mut + " " + weatherStr
//                }
//                
//                if (countElements(mut) > 0){
//                    self.addMessage(mut)
//                }
//            }
//        }
//        
//        socket.on("typing"){ data in
//            
//            if let un = data as? String {
//                
//                for user in self.users {
//                    
//                    if (user.un == un){
//                        user.isTyping = true
//                        self.usersTable.reloadData()
//                    }
//                    
//                }
//            }
//        }
//        
//        socket.on("stopTyping"){ data in
//            if let un = data as? String {
//                
//                for user in self.users {
//                    
//                    if (user.un == un){
//                        user.isTyping = false
//                        self.usersTable.reloadData()
//                    }
//                }
//            }
//        }
        
        
        socket.connect()
    }
    
    

}

