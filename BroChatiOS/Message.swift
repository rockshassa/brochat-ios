//
//  Message.swift
//  BroChatiOS
//
//  Created by Nicholas Galasso on 10/17/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

class Message {

    var sender:User?
    var messageString = ""
    
    convenience init(str:String){
        self.init()
        
        self.messageString = str 
    }
}
