//
//  ChatTableView.swift
//  BroChatiOS
//
//  Created by Nicholas Galasso on 10/17/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import UIKit

class ChatTableView: UITableView, UITableViewDataSource, UITableViewDelegate {

    var data:[Message] = []
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        self.dataSource = self;
        self.delegate = self;
        
        self.registerClass(UITableViewCell.self, forCellReuseIdentifier:"std")
   }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*

    UITableView Data Source
    
    */
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("std", forIndexPath: indexPath)
        
        let message = self.data[indexPath.row]
        
        cell.textLabel?.text = message.messageString;
        
        return cell
    }
    
    /*

    UITableView Delegate
    
    */
    
    
    /*
    
    Chat Handling
    
    */
    
    func addMessage(msg:Message){
        
        self.data.append(msg)
        reloadTableAndScrollToBottom()
        
    }
    
    func reloadTableAndScrollToBottom(){
        print(self.data)
        self.reloadData()
        
        let targetIndex = NSIndexPath(forRow: self.data.count-1, inSection: 0)
        self.scrollToRowAtIndexPath(targetIndex, atScrollPosition:.Bottom, animated: true)
    }
    
    
    
}
