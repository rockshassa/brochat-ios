//
//  File.swift
//  BroChatiOS
//
//  Created by Nicholas Galasso on 10/17/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import Foundation

class User {
    
    var un:String = ""
    var isTyping:Bool = false
    
    convenience init(username:String){
        self.init()
        self.un = username
    }
    
}